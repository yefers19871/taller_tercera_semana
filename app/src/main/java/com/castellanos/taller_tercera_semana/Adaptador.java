package com.castellanos.taller_tercera_semana;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

// el adaptador es el que me toma los datos me los guarda en el cajo y los pasa al recycler vies
public class Adaptador extends RecyclerView.Adapter <Elemento>{


    //crear un cajon y meter los datos en un cajo, pero antes debe saber de  donde va a tomar los datos,

    List<String> elementos;

    //crear un metodo especifico para trabajar con unos datos especificos

    public Adaptador(List<String> datos){
//inicializa el listado de elementos creando el conjunto de datos que estan en la lista
        elementos=datos;
    }

//como crear los datos
    @NonNull
    @Override
    public Elemento onCreateViewHolder (@NonNull ViewGroup parent, int viewType) {

        //creador o inflador de layout traer el objeto del layput para construir los datos LayoutInflater

        LayoutInflater miCreadordeCajones;

        View miCajon;
        miCajon= LayoutInflater.from(parent.getContext()).inflate(R.layout.elemento,parent,false);

        return new Elemento(miCajon);
    }
//muestra datos
    @Override
    public void onBindViewHolder(@NonNull Elemento holder, int position) {

        String miDato= elementos.get(position);
        holder.tvElemento.setText(miDato);

    }
//datos disponibles
    @Override
    public int getItemCount() {
        if (elementos!= null){
            return elementos.size();

        }else
            return 0;
    }
}
