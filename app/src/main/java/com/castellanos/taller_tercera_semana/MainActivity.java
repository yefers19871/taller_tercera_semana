package com.castellanos.taller_tercera_semana;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

//crear un objeto recyclerview
    RecyclerView rvElementos;

    //crear el objeto
    Adaptador miAdaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(R.layout.recyclerone);

        //tener el recyclerview
        rvElementos=findViewById(R.id.tvElemento);

        rvElementos.setLayoutManager(new LinearLayoutManager(this));

        //definir datos pueden obtenerse de diferentes sitios

        //1. datos locales definidos dentro de la app aquí:

        List<String> misDatosLocales= new ArrayList<>();
        misDatosLocales.add("Yefer Alexander");
        misDatosLocales.add("Castellanos Parra");
        misDatosLocales.add("Policía Nacional");


        //tomar los datos desde un array

        List<String> misDatosString= Arrays.asList(getResources().getStringArray(R.array.nombres));



        // crear un robot de la clase adpatador para que tome los datos y los meta en una cajita y los ubique en un recycler view

        miAdaptador= new Adaptador(misDatosLocales);
        rvElementos.setAdapter(miAdaptador);

    }

}